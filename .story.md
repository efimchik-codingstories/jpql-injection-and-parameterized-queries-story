---
focus: src/main/java/com/epam/jpqlinjection/LoginHandler.java
---


### JPQL Injection and Parameterized Queries

That concludes the story of JPQL Injection and Parameterized Queries.

You have seen an example of a simple JPQL Injection attack in action and learned how to prevent it with the help of Parameterized Queries.

See an example of a similar story for SQL Injection in *SQL Injection and Parameterized Queries in Java*.

### The end
