package com.epam.jpqlinjection;

import com.epam.jpqlinjection.entities.User;
import com.epam.jpqlinjection.entitymanagers.EntityManagers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.OptionalLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LoginHandlerTest {

    @Test
    void testLoginHandlerSqlInjection() {

        EntityManager em = new EntityManagers().getEntityManager();
        try {
            LoginHandler loginHandler = new LoginHandler();

            em.getTransaction().begin();
            User edgar = new User("Edgar", "P@wP@w");
            em.persist(edgar);
            em.getTransaction().commit();

            em.getTransaction().begin();
            OptionalLong userId = loginHandler.login(em, "Edgar", "P@wP@w");
            em.getTransaction().commit();

            //assertions pass - password is right
            assertTrue(userId.isPresent());
            assertEquals(edgar.getId(), userId.getAsLong());
        } finally {
            em.close();
        }
    }

}